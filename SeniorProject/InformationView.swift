//
//  InformationView.swift
//  SeniorProject
//
//  Created by Courtney Dittmer on 4/21/20.
//  Copyright © 2020 Courtney Dittmer. All rights reserved.
//

import SwiftUI

struct InformationView: View {
    var body: some View {
        Form {
            Section {
                Text("If you have questions about chemicals, or  the levels are off, call Becky.")
            }
            Section {
                Text("If you have questions about swimmers, or other things that can be handled without Becky, call the front desk,  Extension 104 on the office phone.")
            }
        }
    }
}

struct InformationView_Previews: PreviewProvider {
    static var previews: some View {
        InformationView()
    }
}
