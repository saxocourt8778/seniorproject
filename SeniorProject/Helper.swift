//
//  Helper.swift
//  SeniorProject
//
//  Created by Courtney Dittmer on 5/5/20.
//  Copyright © 2020 Courtney Dittmer. All rights reserved.
//

import Foundation

func ExtractDateAndTime(todayDate: Date) ->(String, String, String, Int){
    // Pull out hour
    // Locals
    var iHour = 0
    var iMinute = 0
    var iSecond = 0
    var iMonth = 0
    var iDay = 0
    var iYear = 0
    var strFullDate = ""
    var strTime = ""
    var strWeekday = ""
    var iWeekdayIndex = 0
    
    let Weekdays = ["", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
    let calendar = Calendar.current

    // *** Get time ***
    iHour = calendar.component(.hour, from: todayDate)
    iMinute = calendar.component(.minute, from: todayDate)
    iSecond = calendar.component(.second, from: todayDate)
    strTime = "\(iHour):\(iMinute):\(iSecond)"
    
    // *** Get date and weekday ***
    iDay = calendar.component(.day, from: todayDate)
    iMonth = calendar.component(.month, from: todayDate)
    iYear = calendar.component(.year, from: todayDate)
    strFullDate = "\(iMonth)-\(iDay)-\(iYear)"
    iWeekdayIndex = calendar.component(.weekday, from: todayDate)
    strWeekday = Weekdays[iWeekdayIndex]
    
    return (strFullDate, strTime, strWeekday, iHour)
}
