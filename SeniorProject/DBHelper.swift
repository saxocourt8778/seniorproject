//
//  DBHelper.swift
//
//  Created by user169053 on 4/6/20.
//  Copyright © 2020 Hastings College. All rights reserved.
//

import Foundation
import SQLite3

class SwimmerType: Identifiable {
    var id: Int = 0
    var name: String = ""
    
    init(initID: Int, initName: String) {
        self.name = initName
        self.id = initID
    }
}

// Function to copy DB file to proper folder so we can modify it, if necessary.
func copyDBIfNecessary(_ DBFileName: String) {
    let fileManager = FileManager.default
    let documentsUrl = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
    guard documentsUrl.count != 0 else {
        return
    }
    
    let finalDatabaseURL = documentsUrl.first!.appendingPathComponent("\(DBFileName)")
    if !( (try? finalDatabaseURL.checkResourceIsReachable()) ?? false) {
        print("DB does not exist in documents folder")

        let databaseInMainBundleURL = Bundle.main.resourceURL?.appendingPathComponent("\(DBFileName)")

        do {
            try fileManager.copyItem(atPath: (databaseInMainBundleURL?.path)!, toPath: finalDatabaseURL.path)
        } catch let error as NSError {
            print("Couldn't copy file to final location! Error:\(error.description)")
        }

    } else {
        print("Database file found at path: \(finalDatabaseURL.path)")
    }
}

class DBHelper {
    init(DBFileName: String) {
        copyDBIfNecessary(DBFileName)
        db = openDatabase(DBFileName)
    }
    
    deinit {
        sqlite3_close(db)
    }
    
    var db: OpaquePointer?
    
    func openDatabase(_ DBFileName: String) -> OpaquePointer? {
        let fileManager = FileManager.default
        let documentsUrl = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        let fileURL = documentsUrl.first!.appendingPathComponent("\(DBFileName)")
        var dbLocal: OpaquePointer? = nil
        if sqlite3_open(fileURL.absoluteString, &dbLocal) == SQLITE_OK {
            print("Successfully opened database connection at \(fileURL.path)")
            return dbLocal
        } else {
            print("Error opening database")
            return nil
        }
    }
    
    func readFromDB() -> [SwimmerType] {
        let queryString = "SElECT * FROM tSwimmerTypes  ORDER BY fTypePriority"
        var queryStatement: OpaquePointer? = nil
        var swimmerTypes: [SwimmerType] = []
        
        if sqlite3_prepare_v2(db, queryString, -1, &queryStatement, nil) == SQLITE_OK {
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                let id = sqlite3_column_int(queryStatement, 0)
                let name = String(cString: sqlite3_column_text(queryStatement, 1))
                swimmerTypes.append(SwimmerType(initID: Int(id), initName: name))
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        
        return swimmerTypes
    }
    
    // Method to insert a name. Return a copy of self, to allow chaining.
    func insertName(_ name: String) -> DBHelper {
        // Set up INSERT query with placeholder for name.
        let queryString = "INSERT INTO Users(Name) VALUES (?)"
        var queryStatement: OpaquePointer? = nil
        
        if sqlite3_prepare_v2(db, queryString, -1, &queryStatement, nil) == SQLITE_OK {
            sqlite3_bind_text(queryStatement, 1, (name as NSString).utf8String, -1, nil)
            if sqlite3_step(queryStatement) == SQLITE_DONE {
                print("Name successfully inserted")
            } else {
                print("Could not insert name")
            }
        } else {
            print("Unable to prepare INSERT statement")
        }
        
        sqlite3_finalize(queryStatement)
        return self
    }
    
    func insertAttendanceRow(_ date: String, _ dayOfWeek: String, _ time: String, _ hour: Int, _ numSwimmers: Int, _ swimmerType: Int)  {
        var blockNum: Int32 = 0
        let queryStr =
            """
            SELECT fBlockID
             FROM tTimeBlocks
             WHERE fStartTime =
               ( SELECT MAX(fStartTime)
                 FROM tTimeBlocks
                 WHERE fStartTime <= \(hour))
            """
        var queryStmt: OpaquePointer? = nil
        
        if sqlite3_prepare_v2(db, queryStr, -1, &queryStmt, nil) == SQLITE_OK {
            while sqlite3_step(queryStmt) == SQLITE_ROW {
                blockNum = sqlite3_column_int(queryStmt, 0)          }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStmt)
        
        let queryString = "INSERT INTO tAttendance(fDate, fDayOfWeek, fTime, fTimeBlock, fNumSwimmers, fSwimmerType) VALUES (?, ?, ?, ?, ?, ?)"
        
        var queryStatement: OpaquePointer? = nil
               
           if sqlite3_prepare_v2(db, queryString, -1, &queryStatement, nil) == SQLITE_OK {
               sqlite3_bind_text(queryStatement, 1, (date as NSString).utf8String, -1, nil)
               sqlite3_bind_text(queryStatement, 2, (dayOfWeek as NSString).utf8String, -1, nil)
               sqlite3_bind_text(queryStatement, 3, (time as NSString).utf8String, -1, nil)
               sqlite3_bind_int(queryStatement, 4, Int32(blockNum))
               sqlite3_bind_int(queryStatement, 5, Int32(numSwimmers))
               sqlite3_bind_int(queryStatement, 6, Int32(swimmerType))
               if sqlite3_step(queryStatement) == SQLITE_DONE {
                   print("Values successfully inserted")
               } else {
                   print("Could not insert values")
               }
           } else {
               print("Unable to prepare INSERT statement")
           }
        
        sqlite3_finalize(queryStatement)
    }
    
    
    func insertOpeningChemRow(_ strDate: String, _ strTime: String, _ poolChlorineLevel: Double, _ poolPHLevel: Double, _ poolCalcium: Int, _ poolAlkalinity: Int, _ poolCombinedChlorine: Double, _ phReadingController: Double, _ ORP: String, _ poolWaterTemperature: String, _ spaChlorineLevel: Double, _ spaPHLevel: Double, _ spaCombinedChlorine: Double, _ spaAlkalinity: Int){
        let queryString = "INSERT INTO tOpeningChemicals(fDate, fTime, fPoolChlorine, fPoolPH, fPoolCalcium, fPoolAlkalinity, fPoolCombChlorine, fControllerPH, fORP, fWaterTemperature, fSpaChlorine, fSpaPH, fSpaCombChlorine, fSpaAlkalinity) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
        
        var queryStatement: OpaquePointer? = nil
               
           if sqlite3_prepare_v2(db, queryString, -1, &queryStatement, nil) == SQLITE_OK {
               sqlite3_bind_text(queryStatement, 1, (strDate as NSString).utf8String, -1, nil)
               sqlite3_bind_text(queryStatement, 2, (strTime as NSString).utf8String, -1, nil)
               sqlite3_bind_double(queryStatement, 3, Double(poolChlorineLevel))
               sqlite3_bind_double(queryStatement, 4, Double(poolPHLevel))
               sqlite3_bind_int(queryStatement, 5, Int32(poolCalcium))
               sqlite3_bind_int(queryStatement, 6, Int32(poolAlkalinity))
               sqlite3_bind_double(queryStatement, 7, Double(poolCombinedChlorine))
               sqlite3_bind_double(queryStatement, 8, Double(phReadingController))
               sqlite3_bind_text(queryStatement, 9, (ORP as NSString).utf8String, -1, nil)
               sqlite3_bind_text(queryStatement, 10, (poolWaterTemperature as NSString).utf8String, -1, nil)
               sqlite3_bind_double(queryStatement,11, Double(spaChlorineLevel))
               sqlite3_bind_double(queryStatement,12, Double(spaPHLevel))
               sqlite3_bind_double(queryStatement, 13, Double(spaCombinedChlorine))
               sqlite3_bind_int(queryStatement, 14, Int32(spaAlkalinity))
            
               if sqlite3_step(queryStatement) == SQLITE_DONE {
                   print("Values successfully inserted")
               } else {
                   print("Could not insert values")
               }
           } else {
               print("Unable to prepare INSERT statement")
           }
        
        sqlite3_finalize(queryStatement)
    }
    
    func insertClosingChemRow(_ strDate: String, _ strTime: String, _ poolChlorineLevel: Double, _ poolPHLevel: Double, _ phReadingController: Double, _ ORP: String, _ poolWaterTemperature: String, _ spaChlorineLevel: Double, _ spaPHLevel: Double){
        let queryString = "INSERT INTO tClosingChemicals(fDate, fTime, fPoolChlorine, fPoolPH, fControllerPH, fORP, fWaterTemperature, fSpaChlorine, fSpaPH) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)"
        
        var queryStatement: OpaquePointer? = nil
               
           if sqlite3_prepare_v2(db, queryString, -1, &queryStatement, nil) == SQLITE_OK {
               sqlite3_bind_text(queryStatement, 1, (strDate as NSString).utf8String, -1, nil)
               sqlite3_bind_text(queryStatement, 2, (strTime as NSString).utf8String, -1, nil)
               sqlite3_bind_double(queryStatement, 3, Double(poolChlorineLevel))
               sqlite3_bind_double(queryStatement, 4, Double(poolPHLevel))
               sqlite3_bind_double(queryStatement, 5, Double(phReadingController))
               sqlite3_bind_text(queryStatement, 6, (ORP as NSString).utf8String, -1, nil)
               sqlite3_bind_text(queryStatement, 7, (poolWaterTemperature as NSString).utf8String, -1, nil)
               sqlite3_bind_double(queryStatement, 8, Double(spaChlorineLevel))
               sqlite3_bind_double(queryStatement, 9, Double(spaPHLevel))
            
               if sqlite3_step(queryStatement) == SQLITE_DONE {
                   print("Values successfully inserted")
               } else {
                   print("Could not insert values")
               }
           } else {
               print("Unable to prepare INSERT statement")
           }
        
        sqlite3_finalize(queryStatement)
    }
    
    func insertDailyChemRow(_ strDate: String, _ strTime: String, _ poolPHLevel: Double, _ poolChlorine: Double, _ spaPHLevel: Double, _ spaChlorine: Double, _ inchesAdded: Int, _ hoursOn: Double){
                  let queryString = "INSERT INTO tDailyChemicals(fDate, fTime, fPoolChlorine, fPoolPH, fWaterInchesAdded, fWaterTimeOn, fSpaPH, fSpaChlorine) VALUES (?, ?, ?, ?, ?, ?, ?, ?)"
         
         var queryStatement: OpaquePointer? = nil
                
            if sqlite3_prepare_v2(db, queryString, -1, &queryStatement, nil) == SQLITE_OK {
                sqlite3_bind_text(queryStatement, 1, (strDate as NSString).utf8String, -1, nil)
                sqlite3_bind_text(queryStatement, 2, (strTime as NSString).utf8String, -1, nil)
                sqlite3_bind_double(queryStatement, 3, Double(poolChlorine))
                sqlite3_bind_double(queryStatement, 4, Double(poolPHLevel))
                sqlite3_bind_int(queryStatement, 5, Int32(inchesAdded))
                sqlite3_bind_double(queryStatement, 6, Double(hoursOn))
                sqlite3_bind_double(queryStatement, 7, Double(spaPHLevel))
                sqlite3_bind_double(queryStatement, 8, Double(spaChlorine))
                if sqlite3_step(queryStatement) == SQLITE_DONE {
                    print("Values successfully inserted")
                } else {
                    print("Could not insert values")
                }
            } else {
                print("Unable to prepare INSERT statement")
            }
         
         sqlite3_finalize(queryStatement)
    }
}
