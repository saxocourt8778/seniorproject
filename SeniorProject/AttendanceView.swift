//
//  AttendanceView.swift
//  SeniorProject
//
//  Created by Courtney Dittmer on 4/16/20.
//  Copyright © 2020 Courtney Dittmer. All rights reserved.
//

import SwiftUI
import Combine

struct AttendanceView: View {
    
    var dateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateStyle = .full
        return formatter
    }

    @State var todayDate = Date()
    @State var numSwimmers = 0
    @State var swimmerType = 0
    @State var showingSubmitAlert = false
    @State var showValueAlertBox = false

    static let swimmerTypeList = DBHelper(DBFileName: "AquaticsLogs.db").readFromDB()
    
    
    
    var body: some View {
        Form {
            Section {
                DatePicker(selection: $todayDate, in: ...Date()) {
                    Text("Date and Time:")
                }
            }
            
            Section {
                    Picker("Swimmer Type", selection: $swimmerType) {
                            ForEach(0 ..< Self.swimmerTypeList.count) {
                                Text(Self.swimmerTypeList[$0].name)
                            }
                    }
                
            }
            
            Section {
                if showValueAlertBox {
                    HStack {
                        Spacer()
                        Text("Please enter a number of swimmers greater than zero.").font(.caption).bold().foregroundColor(.red)
                        Spacer()
                    }
                }
                // Ideally could click and type in number, but will add later
                Stepper(value: $numSwimmers, in: 0...100, label: { Text("Number of Swimmers:  \(numSwimmers)")})
            }
            
            Section {
                HStack {
                    Spacer()
                    Button("Submit") {
                        // Enter function to submit and record values entered by user
                        if self.numSwimmers >= 1 {
                            self.SubmitAttendanceValues()
                            self.showingSubmitAlert.toggle()
                        } else {
                            self.showValueAlertBox = true
                        }
                    }
                    Spacer()
                }
            }
            }.alert(isPresented: $showingSubmitAlert){
                Alert(title: Text("Values Recorded!"), message: Text("\(numSwimmers) \(Self.swimmerTypeList[swimmerType].name) swimmer(s) recorded."), dismissButton: .default(Text("OK"), action: {self.RestoreDefaults()}))}
    }
    //Function to submit values from page
    func SubmitAttendanceValues(){
        let (strDate, strTime, strWeekday, iHour) = ExtractDateAndTime(todayDate: Date())
        // List of swimmer types is an array starting at index of 0, but DB auto-numerated, and starts at 1
        swimmerType += 1
        // Call function to submit values
        DBHelper(DBFileName: "AquaticsLogs.db").insertAttendanceRow(strDate, strWeekday, strTime, iHour, numSwimmers, swimmerType)
    }
    
    // Function to restore the default values after submit button pressed.
    func RestoreDefaults(){
        todayDate = Date()
        numSwimmers = 0
        swimmerType = 0
        showingSubmitAlert = false
        self.showValueAlertBox = false
    }
    
    
    
}



struct AttendanceView_Previews: PreviewProvider {
    static var previews: some View {
        AttendanceView()
    }
}
