//
//  DailyChemicalView.swift
//  SeniorProject
//
//  Created by Courtney Dittmer on 4/16/20.
//  Copyright © 2020 Courtney Dittmer. All rights reserved.
//

import SwiftUI

struct ChemicalView: View {
    
    var dateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateStyle = .full
        return formatter
    }
    
    static let LogTypes = [ "Opening Chemicals", "Closing Chemicals"]

    // General variables
    @State private var todayDate = Date()
    @State private var logType = 0
    @State private var bOpeningorClosing = false
    @State private var btestingCalandAlk = false
    
    // Pool Stuff
    @State private var poolChlorineLevel = 5.0
    @State private var poolPHLevel = 7.0
    @State private var bGuttersOn = false
    @State private var hoursOn = 0.0
    @State private var inchesAdded = 0

    // Spa Stuff
    @State private var spaChlorineLevel = 5.0
    @State private var spaPHLevel = 7.0
    @State private var spaTestChemicals = false
    @State private var spaCombinedChlorine = 0.0
    @State private var spaAlkalinity = 70
    
    // Opening and closing stuff
    @State private var poolCombinedChlorine = 0.0
    @State private var poolAlkalinity = 70
    @State private var poolCalcium = 250
    @State private var phReadingController = 7.0
    @State private var ORP = ""
    @State private var poolWaterTemperature = ""
    @State private var guardName = ""
    @State private var showingSubmitAlert = false
    
    
    var body: some View {
        Form {
            Section {
                Toggle(isOn: $bOpeningorClosing.animation()){
                    Text("Opening or Closing?")
                }
                
                if bOpeningorClosing {
                    Picker("Which one?", selection: $logType) {
                        ForEach(0 ..< Self.LogTypes.count) {
                            Text(Self.LogTypes[$0])
                            
                        }
                    
                    }
                    TextField("Your Name", text: $guardName)
                }
            }
            
            Section {
                DatePicker(selection: $todayDate, in: ...Date()) {
                    Text("Date and Time:")
                }
            }
            
            // Pool Section
                
            Section (header: Text("Pool Chemicals").font(.headline)){
                Stepper(value: $poolChlorineLevel, in: 0.0...10.0, step: 0.1, label: { Text("Chlorine Level:  \(poolChlorineLevel, specifier: "%.1f")")})

                Stepper(value: $poolPHLevel, in: 0.0...10.0, step: 0.1, label: { Text("PH Level:  \(poolPHLevel, specifier: "%.1f")")})
            
            
                if bOpeningorClosing {
                    
                    if self.logType == 0 {
                        Toggle(isOn: $btestingCalandAlk.animation()){
                            Text("Testing Calcium, Alkalinity, Combined Chlorine?")}
                    }
                    
                    if btestingCalandAlk {
                        
                        Stepper(value: $poolAlkalinity, in: 0...400, step: 10, label: { Text("Alkalinity:  \(poolAlkalinity)")})

                        Stepper(value: $poolCalcium, in:  0...400, step: 10, label: { Text("Calcium:  \(poolCalcium)")})
                        
                        Stepper(value: $poolCombinedChlorine, in: 0.0...10.0, step: 0.1, label: { Text("Combined Chlorine:  \(poolCombinedChlorine, specifier: "%.1f")")})
                    }
            
                    Stepper(value: $phReadingController, in:  0...14, step: 0.1, label: { Text("PH Reading on Controller:  \(phReadingController, specifier: "%.1f")")})
                    
                    // Must be converted to number (double)
                    TextField("ORP Reading: ", text: $ORP)
                    
                    TextField("Water Temperature: ", text: $poolWaterTemperature)
                } else {
                
                    Toggle(isOn: $bGuttersOn.animation()){
                        Text("Turned on Water?") }
                
                    if bGuttersOn {
                       Stepper(value: $inchesAdded, in: 0...10, step: 1, label: { Text("Approximate Inches Added:  \(inchesAdded)")})

                       Stepper(value: $hoursOn, in: 0...10, step: 0.5, label: { Text("Approximate Hours On:  \(hoursOn, specifier: "%.1f")")})
                    }
                }
            }
            
            // Spa Section
            Section (header: Text("Spa Chemicals").font(.headline)){
                Toggle(isOn: $spaTestChemicals.animation()){
                    Text("Testing Spa Chemicals")
                }
                
                if spaTestChemicals {
                    Stepper(value: $spaChlorineLevel, in: 0.0...10.0, step: 0.1, label: { Text("Chlorine Level:  \(spaChlorineLevel, specifier: "%.1f")")})
                    
                    Stepper(value: $spaPHLevel, in: 0.0...10.0, step: 0.1, label: { Text("PH Level:  \(spaPHLevel, specifier: "%.1f")")})
                    if bOpeningorClosing {
                        if btestingCalandAlk {
                            Stepper(value: $spaCombinedChlorine, in: 0.0...10.0, step: 0.1, label: { Text("Combined Chlorine:  \(spaCombinedChlorine, specifier: "%.1f")")})
                            
                            Stepper(value: $spaAlkalinity, in: 0...400, step: 10, label: { Text("Alkalinity:  \(spaAlkalinity)")})
                        }
                    } // end of first if
                } // end of nested if
            } // end of spa section
            
            Section {
                HStack {
                    Spacer()
                    Button("Submit") {
                        // Enter function to submit and record values entered by user
                        self.SubmitChemicalValues()
                        self.showingSubmitAlert = true
                    }
                    Spacer()
                }
            } // end of button section
            
        }.alert(isPresented: $showingSubmitAlert){
        Alert(title: Text("Values Recorded!"), message: Text("Chemical values recorded. Press OK to continue."), dismissButton: .default(Text("OK"), action: {self.RestoreDefaults()}))}
    }
    
    //  Do stuff here
    //Function to submit values from page
    func SubmitChemicalValues(){
        let (strDate, strTime, _, _) = ExtractDateAndTime(todayDate: Date())
        // Call function to submit values
        // If spa chemicals not tested,  set values to 0
        if spaTestChemicals == false{
            spaPHLevel = 0.0
            spaChlorineLevel = 0.0
        }
        // If Calc and Alk not tested, set values to 0
        if btestingCalandAlk == false {
            spaAlkalinity = 0
            poolCalcium = 0
            poolAlkalinity = 0
        }
        // Find appropriate table to insert values. If values were not changed, their defaults will be inserted
        if self.bOpeningorClosing {
            if self.logType == 0 {
                //Opening Chemicals Selecte./d
                DBHelper(DBFileName: "AquaticsLogs.db").insertOpeningChemRow(strDate, strTime, poolChlorineLevel, poolPHLevel, poolCalcium, poolAlkalinity, poolCombinedChlorine, phReadingController, ORP, poolWaterTemperature, spaChlorineLevel, spaPHLevel, spaCombinedChlorine, spaAlkalinity)
            } else {
                // Closing Chemicals Selected
                DBHelper(DBFileName: "AquaticsLogs.db").insertClosingChemRow(strDate, strTime, poolChlorineLevel, poolPHLevel, phReadingController, ORP, poolWaterTemperature, spaChlorineLevel, spaPHLevel)
            }
        } else {
            // Daily Chemicals Selected
            DBHelper(DBFileName: "AquaticsLogs.db").insertDailyChemRow(strDate, strTime, poolPHLevel, poolChlorineLevel, spaPHLevel, spaChlorineLevel, inchesAdded, hoursOn)
        }
    }
        
    func RestoreDefaults() {
        // On submit, restore the default variable values
        // General variables
        logType = 0
        bOpeningorClosing = false
        btestingCalandAlk = false
        
        // Pool Stuff
        poolChlorineLevel = 5.0
        poolPHLevel = 7.0
        bGuttersOn = false
        hoursOn = 0.0
        inchesAdded = 0

        // Spa Stuff
        spaChlorineLevel = 5.0
        spaPHLevel = 7.0
        spaTestChemicals = false
        spaCombinedChlorine = 0.0
        spaAlkalinity = 70
        
        // Opening and closing stuff
        poolCombinedChlorine = 0.0
        poolAlkalinity = 70
        poolCalcium = 250
        phReadingController = 7.0
        ORP = ""
        poolWaterTemperature = ""
        guardName = ""
        showingSubmitAlert = false
    }
    
    
    
}

struct ChemicalView_Previews: PreviewProvider {
    static var previews: some View {
        ChemicalView()
    }
}
