//
//  ContentView.swift
//  SeniorProject
//
//  Created by Courtney Dittmer on 4/9/20.
//  Copyright © 2020 Courtney Dittmer. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView {
            TabView {
               AttendanceView()
                   .tabItem {
                       Image("Swimmer")
               }
               ChemicalView()
                   .tabItem {
                       Image("Chemical")
               }
               InformationView()
                    .tabItem {
                        Image(systemName: "list.dash")
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
